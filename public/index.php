<?php
include("../include/menu.php");
require_once('../include/connexion.php');
require_once('../include/fonction.php');
//session_start();

// Afficher le formulaire de connexion
$formulaire = formulaireLogin();
echo $formulaire;
afficheMessages();
global $bdd;

// Gestion de la connexion
if (isset($_POST['Connexion'])) {
    try {
        $requete = $bdd->prepare('SELECT code, password, ville, fournisseur FROM utilisateur WHERE login = LOWER(?)');
        $requete->execute(array(strtolower($_POST['login'])));
        $utilisateur = $requete->fetch();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage() . "<br/>";
        die();
    }

    // Si le login est inconnu
    if (empty($_POST['login'])) {
        // On met un message d'erreur générique
        $_SESSION['MSG_KO'] = "Identification invalide";
    } else {
        // Test du mot de passe saisi


        if (password_verify($_POST['password'], $utilisateur['password'])) {
            $_SESSION['login'] = $_POST['login'];
            $_SESSION['code'] = $utilisateur['code'];
            $_SESSION['ville'] = $utilisateur['ville'];
            $_SESSION['fournisseur'] = $utilisateur['fournisseur'];

        } else {
            // On met le même message d'erreur que pour "login raté"
            $_SESSION['MSG_KO'] = "Identification invalide";
        }
    }
}


?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Index</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
<h1>Mon super TP</h1>

<!-- Insérez ici votre contenu HTML supplémentaire -->

</body>
</html>
