<?php
require_once('../include/fonction.php');
require_once('../include/connexion.php');
$id = (isset($_GET['id'])) ? $_GET['id'] : 0;
if ($id == 0) { #Si l'id est =0(il le sera forcément car on a encore rien sélectionné), nous renvoie sur listeFournisseur.php, où en sélectionnant un fournisseur ça donnera un id à fournisseur.php
    header("Location:listeFournisseur.php"); #/!\Attention au chemin
    die();
}
global $bdd;
?>
<?php

include("../include/menu.php");
require_once('../include/connexion.php');
require_once('../include/fonction.php');
afficheMessages();


            if(isset($_POST['Suprimer']) and  $_SESSION['fournisseur'] = 1) {
                try {
                    $requete = $bdd->prepare('delete from fournisseur where code = :code');
                    $requete->execute(array('code' => $id));
                    $_SESSION['success'] = "supression Ok ";
                }catch (PDOException $e) {
                        print "Erreur !: " . $e->getMessage() . "<br/>";
                        die();
                    }

                header("Location:listeFournisseur.php");
            }


        // Si on à cliqué sur "Modifier"

        if((isset($_POST['Modifier']) or isset($_POST['creer'])) and $_SESSION['fournisseur'] = 1) {
            $nom0 = $_POST['nom'];
            $requete = $bdd->prepare('select count(*) as cpt from fournisseur where nom = :nom');
            $requete->execute(array( 'nom' => $nom0 ));
            $compteur = $requete->fetch();

            if (empty($_POST['adresse1'])) {
                $_SESSION['MSG_ADD'] = "L'adresse est obligatoire";
            }
            elseif (strlen($_POST['nom']) < 3) {
                $_SESSION['MSG_NAME'] = "Le nom ne peut pas être moins que 3";
            }
            elseif($compteur['cpt'] == 1) {
                $_SESSION['MSG_NAME'] = "le nom (".$_POST['nom'].") est déjà pris";
                }

            elseif(isset($_POST['Modifier']) and $_SESSION['fournisseur'] = 1 ) {
                try {
                    $requete = $bdd->prepare('update fournisseur
                        set nom = :nom
                        , adresse1 = :adresse1
                        , adresse2 = :adresse2
                        , ville = :ville
                        , contact = :contact
                        , civilite = :civilite
                        where code = :code');

                    $requete->execute(array('nom' => $_POST['nom']
                    , 'adresse1' => $_POST['adresse1']
                    , 'adresse2' => $_POST['adresse2']
                    , 'ville' => $_POST['ville']
                    , 'contact' => $_POST['contact']
                    , 'civilite' => $_POST['civilite']
                    , 'code' => $_POST['code']
                    ));
                } catch (PDOException $e) {
                    print "Erreur !: " . $e->getMessage() . "<br/>";
                    die();
                }
                $_SESSION['MSG_OK'] = "Modification bien enregistrée";

            }
            elseif(isset($_POST['creer']) ) {
            try {

                $requete = $bdd->prepare('insert into fournisseur (nom, adresse1, adresse2,
            ville, contact, civilite) values(:nom, :adresse1, :adresse2, :ville, :contact, :civilite)');
                $requete->execute(array( 'nom' => $_POST['nom']
                , 'adresse1' => $_POST['adresse1']
                , 'adresse2' => $_POST['adresse2']
                , 'ville' => $_POST['ville']
                , 'contact' => $_POST['contact']
                , 'civilite' => $_POST['civilite']
            ));
                // on met message de succès
                $_SESSION['MSG_OK'] = "Création bien enregistrée";
                $_SESSION['SUCCESS'] = "0";

            } catch (PDOException $e) {
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();
            }

            // on récupère l'id du fournisseur créé
            $id = $bdd->lastInsertId();}
            }

        try {
            $requete = $bdd->prepare('select nom, adresse1, adresse2, ville, contact,
            civilite, code from fournisseur where code = ?');
            $requete->execute(array($id));
                $fournisseur = $requete->fetch();
        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
        die();
    }
            afficheMessages();



        if(isset($_POST['Annuler'])) {
                header("Location:listeFournisseur.php");
            }


?>

<!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta charset="utf-8">
            <title>fournisseur <?php echo $fournisseur['nom']; ?></title>
            <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        </head>
     <body>
            <div class="container">



                <?php if(!isset($_POST['Nouveau']) and !isset($_POST['creer']) ) { ?>
                    <h1>Fournisseur : <?php echo $fournisseur['nom']; ?></h1>
                    <?php
                } else { ?>
                    <h1>Nouveau fournisseur : </h1>
                <?php } ?>

                    <form method="post">
                <div class="mb">
                    <label for="nom" class="form-label">Nom : </label>
                    <input type="text" class="form-control" id='nom' name="nom" aria-describedby="emailHelp" value="<?php echo $fournisseur['nom'] ?>">
                </div>

                <div class="mb">
                    <label for="adresse1" class="form-label">Addresse 1 : </label>
                    <input type="text" class="form-control" id='adresse1' name='adresse1' aria-describedby="emailHelp" value="<?php echo $fournisseur['adresse1'] ?>">
                </div>
                <div class="mb">
                    <label for="adresse2" class="form-label">Addresse 2 : </label>
                    <input type="text" class="form-control" id="adresse2"name='adresse2' aria-describedby="emailHelp" value="<?php echo $fournisseur['adresse2'] ?>">
                </div>
                <div class="mb">

                    <div class="mb">
                    <label class="ville"id='ville' name='ville' for="ville">Ville : </label>
                    <div class="col-sm-10"> 
                        <?php $st11 = selectVille('ville', $fournisseur['ville']);  
                        echo $st11?>
                    </div>
                    </div>

                </div>
                <div class="mb">
                    <label for="contact" class="form-label">Contact : </label>
                    <input type="text" class="form-control" id="contact"name='contact' aria-describedby="emailHelp" value="<?php echo $fournisseur['contact'] ?>">
                </div>

                <div class="formulaire">
                    <div class="form-group row">
                    <label class="civilite" id='civilite'name='civilite' for="civilite">Civilité</label>
                    <div class="col-sm-10">
                        <?php echo selectCivilite('civilite', $fournisseur['civilite']); ?>
                    </div>

                    <div class="mb">
                    <label for="code" class="form-label"></label>
                    <input type="hidden" class="form-control" id='code' name="code" aria-describedby="emailHelp" value="<?php echo $fournisseur['code'] ?>">
                </div>
                        <?php if((!isset($_POST['Nouveau']) and !isset($_POST['creer'])) or isset($_SESSION['SUCCESS'])) { ?>
                        <div class="form-group row float-right">

                                <input type="submit" class="btn btn-default" name="Annuler"
                                value="Annuler" >
                            <?php if($_SESSION['fournisseur'] == 1){?>
                                <input type="submit" class="btn btn-primary" name="Modifier"
                                       value="Modifier">
                                <input type="submit"  class="confirm btn btn-danger" name="Suprimer"
                                       value="Suprimer">
                            <?php }?>
                        </div>
                        <?php
                        unset($_SESSION['SUCCESS']);
                        } else { ?>
                        <div class="form-group row float-right">

                            <input type="submit" class="btn btn-default" name="Annuler"
                                   value="Annuler" >
                            <input type="submit" class="btn btn-primary" name="creer"
                                   value="creer">

                        </div>
                        <?php } ?>


                    </div>
                    </div>
                </form>
            </div>
    </body>
</html>
<script src="../node_modules/jquery/dist/jquery.js"></script>
<script>
    $(function() {
        $('.confirm').click(function() {
            return window.confirm("Êtes-vous sur ?");
        });
    });

</script>
