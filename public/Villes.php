<?php
session_start() ;
require_once('../include/connexion.php');
$id = (isset($_GET['id']))?$_GET['id']:0;
if($id == 0 and !isset($_POST['Nouveau']) and !isset($_POST['creer'])) {
    header("Location:listeVille.php");
    die();
}
global $bdd;
?>
<?php

        include("../include/menu.php");
        require_once('../include/connexion.php');
        require_once('../include/fonction.php');
        afficheMessages();


        //Suprimer une ville
        if(isset($_POST['Suprimer']) and $_SESSION['ville'] = 1) {
            try {
                // Delete the child records first
                $requete = $bdd->prepare('delete from fournisseur where ville = :code');
                $requete->execute(array('code' => $id));

                // Then delete the parent record
                $requete = $bdd->prepare('delete from ville where code = :code');
                $requete->execute(array('code' => $id));

                header("Location:listeVille.php");
                $_SESSION['success'] = "supression Ok ";
                echo '<div class="alert alert-success">' . $_SESSION['success'] . '</div>';

            } catch (PDOException $e) {
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();



        }}

        if((isset($_POST['Modifier'])  or isset($_POST['creer'])) and $_SESSION['ville'] = 1) {

            $allowedValues = array('France', 'Andorre', 'Monaco');
            $nom0 = $_POST['nom'];
            $requete = $bdd->prepare('select count(*) as cpt from ville where nom = :nom');
            $requete->execute(array( 'nom' => $nom0 ));
            $compteur = $requete->fetch();

            if ((!is_numeric($_POST['codepostal']) || ((strlen(strval($_POST['codepostal']))) != 5))) {
                $_SESSION['MSG_CP'] = "Le Codepostal est oblogatoirement composer de 5 numero";
            }
            elseif (strlen($_POST['nom']) <= 2) {
                $_SESSION['MSG_NAME'] = "Le nom ne peut pas être moins que 2";
            }
            elseif (!in_array(($_POST['pays']), $allowedValues)) {
                $_SESSION['MSG_PAYS'] = "Le Pays doit etre France, Andorre, Monaco";
            }
            elseif($compteur['cpt'] == 1) {
                $_SESSION['MSG_NAME'] = "le nom (".$_POST['nom'].") est déjà pris";
                }

            elseif(isset($_POST['Modifier']) and $_SESSION['ville'] = 1 ) {
                try {
                    $requete = $bdd->prepare('update ville
                                    set nom = :nom
                                    , codepostal = :codepostal
                                    , pays = :pays
                                    where code = :code');

                    $requete->execute(array('nom' => $_POST['nom']
                    , 'codepostal' => $_POST['codepostal']
                    , 'pays' => $_POST['pays']
                    , 'code' => $id
                    ));
                } catch (PDOException $e) {
                    print "Erreur !: " . $e->getMessage() . "<br/>";
                    die();
                }
                $_SESSION['MSG_OK'] = "Modification bien enregistrée";
            }
            elseif(isset($_POST['creer']) and $_SESSION['ville'] = 1 ) {
                try {

                    $requete = $bdd->prepare('insert into ville (nom, codepostal, pays) values(:nom, :codepostal, :pays)');
                    $requete->execute(array( 'nom' => $_POST['nom']
                    , 'codepostal' => $_POST['codepostal']
                    , 'pays' => $_POST['pays']
                    ));
                    // on met message de succès
                    $_SESSION['MSG_OK'] = "Création bien enregistrée";
                    $_SESSION['SUCCESS'] = "0";

                } catch (PDOException $e) {
                    print "Erreur !: " . $e->getMessage() . "<br/>";
                    die();
                }

                // on récupère l'id du fournisseur créé
                $id = $bdd->lastInsertId();}

            afficheMessages();
        }
            try {
                $requete = $bdd->prepare('select code, nom, codepostal, pays from ville where code =?');
                $requete->execute(array($id));
                $ville = $requete->fetch();
            } catch (PDOException $e) {
                print "Erreur !: " . $e->getMessage() . "<br/>";
                die();
            }
        if(isset($_POST['Annuler'])) {
            header("Location:listeVille.php");
        }

?>

<!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>ville <?php echo $ville['nom']; ?></title>
            <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
            <link href="../css/style.css" rel="stylesheet">
        </head>
        <body>
            <div class="container">
              <h1>ville <?php echo $ville['nom']; ?></h1>

              <form method="post">
                    <div class="formulaire">
                        <label for="examplenom" class="form-label">Nom</label>
                        <input type="text" class="form-control" id='nom' name="nom" aria-describedby="emailHelp" value="<?php echo $ville['nom'] ?>">
                    </div>

                    <div class="formulaire">
                        <label for="examplecodepostal" class="form-label">Code postal</label>
                        <input type="text" id="codepostal" name="codepostal" class="form-control" value="<?php echo $ville['codepostal'] ?>">
                    </div>

                    <div class="formulaire">
                        <label for="examplepays" class="form-label">Pays</label>
                        <input type="text" id="pays" name="pays" class="form-control" value="<?php echo $ville['pays'] ?>">
                    </div>

                  <?php if((!isset($_POST['Nouveau']) and !isset($_POST['creer'])) or isset($_SESSION['SUCCESS'])) { ?>
                      <div class="form-group row float-right">

                          <input type="submit" class="btn btn-default" name="Annuler"
                                 value="Annuler" >
                          <?php if($_SESSION['ville'] == 1){?>
                          <input type="submit" class="btn btn-primary" name="Modifier"
                                 value="Modifier">
                          <input type="submit"  class="confirm btn btn-danger" name="Suprimer"
                                 value="Suprimer">
                          <?php } ?>
                      </div>
                      <?php
                      unset($_SESSION['SUCCESS']);
                  } elseif($_SESSION['ville'] == 1) { ?>
                      <div class="form-group row float-right">

                          <input type="submit" class="btn btn-default" name="Annuler"
                                 value="Annuler" >
                          <input type="submit" class="btn btn-primary" name="creer"
                                 value="creer">

                      </div>
                  <?php } ?>
            </form>
            </div>

    </body>
</html>
<script src="../node_modules/jquery/dist/jquery.js"></script>
<script>
    $(function() {
        $('.confirm').click(function() {
            return window.confirm("Êtes-vous sur ?");
        });
    });

</script>