<?php
require('fonction.php');
//session_start();
if (isset($_POST['Deconnexion'])) {
    unset($_SESSION['login']);
    unset($_SESSION['code']);
    unset($_SESSION['ville']);
    unset($_SESSION['fournisseur']);
    $_SESSION['MSG_OK'] = "Vous n'êtes plus connecté";
}
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">

                <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="https://localhost/mdprojet/public/index.php">
                <img src="http://localhost/mdprojet/image/vehiclelogo.jpg" width="60px"></a>
                </li>

                <li class="nav-item">
                <a class="nav-link" <?php echo menuActif('fournisseur'); ?> href="https://localhost/mdprojet/public/listeFournisseur.php">Fournisseurs</a>
                </li>

                <li class="nav-item">
                <a class="nav-link"  <?php echo menuActif('ville'); ?> href="https://localhost/mdprojet/public/listeVille.php">Villes</a>
                </li>

            </ul>
            <form class="d-flex">
                <input class="form-control me-2" type="search" placeholder="Rechercher" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Rechercher</button>
            </form>
            </div>
        </div>
        </nav>
